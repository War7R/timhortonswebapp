package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	@Test
	public void testDrinksRegular() {
        MealsService mealService = new MealsService( );
        MealType mealType = MealType.DRINKS;

        List<String> mealTypes = mealService.getAvailableMealTypes( mealType );
		assertTrue("Invalid list for drinks",
				(!(mealTypes.isEmpty())));
				
	}

	@Test
	public void testDrinksException(){
		MealsService mealService = new MealsService( );
        MealType mealType = null;

        List<String> mealTypes = mealService.getAvailableMealTypes( mealType );
        
		assertFalse("Invalid list for drinks",
				
				!(mealTypes.get(0).equals("No Brand Available")));
	}
	
	@Test
	public void testDrinksBoundaryIn(){
		 MealsService mealService = new MealsService( );
	        MealType mealType = MealType.DRINKS;

	        List<String> mealTypes = mealService.getAvailableMealTypes( mealType );
		assertTrue("Invalid list for drinks",
				mealTypes.size() > 3);
	}
	
	@Test
	public void testDrinksBoundaryOut(){
		
		MealsService mealService = new MealsService( );
        MealType mealType = null;

        List<String> mealTypes = mealService.getAvailableMealTypes( mealType );
        
		assertFalse("Invalid list for drinks",
				mealTypes.size() > 1);
	}
}
